import {NextResponse} from "next/server";
import {prisma} from "@/lib/prisma";

export async function GET(request: Request, response: Response) {
    try {
        return NextResponse.json({message: "OK"}, {status: 200})
    } catch (err) {
        return NextResponse.json({message: "Error"}, {status: 500})
    }
}

export async function POST(req:Request, res:Response){

    // We must not initiate client here
    // I'm initiating it globally in lib folder then importing the variable here to use
    // const prisma = new PrismaClient()

    req.json().then( async (r) => {
        try {
            await prisma.user.create({data: r})
        } catch (err) {
            console.log(err)
        }
    })

    return NextResponse.json({message: "OK"}, {status: 200})
}
